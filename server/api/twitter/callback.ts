export default defineEventHandler(async (event) => {
    const body = await readBody(event)
    const response = await $fetch('https://api.twitter.com/oauth/access_token', {
      method: 'POST',
      body: new URLSearchParams(body).toString(),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    })
    return response
  })
  