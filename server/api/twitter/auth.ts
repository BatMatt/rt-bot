export default defineEventHandler(async (event) => {
    const config = useRuntimeConfig()
  
    // Construction correcte de l'en-tête OAuth
    const oauth = {
      oauth_callback: config.public.twitterCallbackUrl,
      oauth_consumer_key: config.public.twitterApiKey,
      oauth_nonce: Math.random().toString(36).substring(2),
      oauth_signature_method: 'HMAC-SHA1',
      oauth_timestamp: Math.floor(Date.now() / 1000).toString(),
      oauth_version: '1.0'
    }
  
    const baseString = `POST&${encodeURIComponent('https://api.twitter.com/oauth/request_token')}&` +
      `${encodeURIComponent(Object.keys(oauth).sort().map(key => `${key}=${encodeURIComponent(oauth[key])}`).join('&'))}`
  
    const signingKey = `${encodeURIComponent(config.public.twitterApiSecret)}&`
    const crypto = await import('crypto')
    const oauthSignature = crypto.createHmac('sha1', signingKey).update(baseString).digest('base64')
  
    const authorizationHeader = `OAuth ${Object.entries({ ...oauth, oauth_signature: oauthSignature })
      .map(([key, value]) => `${encodeURIComponent(key)}="${encodeURIComponent(value)}"`).join(', ')}`
  
    try {
      const response = await $fetch('https://api.twitter.com/oauth/request_token', {
        method: 'POST',
        headers: {
          'Authorization': authorizationHeader,
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      })
  
      const params = new URLSearchParams(response)
      const oauthToken = params.get('oauth_token')
  
      if (oauthToken) {
        return sendRedirect(event, `https://api.twitter.com/oauth/authenticate?oauth_token=${oauthToken}`)
      } else {
        return { error: 'Failed to obtain request token from Twitter' }
      }
    } catch (error) {
      return { error: error.message }
    }
  })